import math
import numpy as np
import cv2
import math
import pydirectinput


# Membuka kamera 0
cap = cv2.VideoCapture(0)
while True:

    # Membaca kamera
    succes, img = cap.read()

    # buat ROI
    cv2.rectangle(img, (350, 50), (560, 240), (0, 255, 0), 0)
    crop_image = img[50:240, 350:560]


    #image processing
    gray = cv2.cvtColor(crop_image, cv2.COLOR_BGR2GRAY)
    (t, threshold ) = cv2.threshold(gray, 120, 255, cv2.THRESH_BINARY)
    thresh_blur = cv2.medianBlur(threshold, 15, 0)
    cv2.imshow("thresh_blur", thresh_blur)      


    # definisikan kernel untuk operasi morfologi
    kernel = np.ones((5,5))

    closed = cv2.morphologyEx(thresh_blur, cv2.MORPH_CLOSE, kernel)  # Lakukan Closing

    filtered = cv2.GaussianBlur(closed, (3,3), 0)           # gaussian blur

    ret, thresh = cv2.threshold(filtered, 127, 255, 0)      # thresholding

    # Cari kontur
    contours, hierachy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    try:
        # cari kontur terbesar
        contour = max(contours, key=lambda x:cv2.contourArea(x))

        #buat bounding box di sekitar contour
        x, y, w, h = cv2.boundingRect(contour)
        cv2.rectangle(crop_image, (x, y), (x+w, y+h), (0,0,255), 0)
    
        # cari convex hull
        hull = cv2.convexHull(contour)

        # Gambar kontur
        canvas = np.zeros(crop_image.shape, np.uint8)
        cv2.drawContours(canvas, [contour], -1, (0,255,0), 0)
        cv2.drawContours(canvas, [hull], -1, (0,255,0), 0)
        cv2.imshow("canvas", canvas)

        # Convexity defects
        hull = cv2.convexHull(contour, returnPoints=False)
        defects = cv2.convexityDefects(contour, hull)

        jum_defects = 0

        for i in range(defects.shape[0]):
            s, e, f, d= defects[i, 0]
            start = tuple(contour[s][0])
            end = tuple(contour[e][0])
            far = tuple(contour[f][0])

            a = math.sqrt((end[0] - start[0]) **2 + (end[1] - start[1]) **2)
            b = math.sqrt((far[0] - start[0]) **2 + (far[1] - start[1]) **2)
            c = math.sqrt((end[0] - far[0]) **2 + (end[1] - far[1]) **2)
            angle = (math.acos((b**2 + c**2 - a**2) / (2*b*c)) *180)/ math.pi

            if angle <= 90:
                jum_defects += 1
                cv2.circle(crop_image, far, 1, (0,0,255), 2)
            cv2.line(crop_image, start, end, (0,255,0), 2)

        if jum_defects > 0:
            pydirectinput.keyDown('a')
            cv2.putText(img, "lompat", (50, 50), cv2.FONT_HERSHEY_COMPLEX, 2, (0,255,0), 2)
        
        else:
            pydirectinput.keyDown('s')

        pydirectinput.keyDown('space')
    
        cv2.waitKey(1)
        if cv2.waitKey(1) ==ord('q'):
            break

    except Exception as e:
        pass
    
    cv2.imshow("Camera", img)
    cv2.waitKey(1)
    if cv2.waitKey(1) ==ord('q'):
        break
